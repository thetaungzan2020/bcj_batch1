const navbutton=document.querySelector('.navbuttons');
navbutton.addEventListener('click',function(){
    navbutton.classList.toggle('changes');
});

const navbar=document.querySelector('.navbar');
window.onscroll=function(){
    const getscroll=window.scrollY;
    if(getscroll>=20) {
        navbar.classList.add('navmenus');
    }else {
        navbar.classList.remove('navmenus');
    }
};

const gallerylist=document.querySelectorAll('.gallerylist');
const filternews=document.querySelectorAll('.filter.new');
const filterfrees=document.querySelectorAll('.filter.free');
const filterpros=document.querySelectorAll('.filter.pro');

gallerylist.forEach(gallery=>{
   const datafilter=gallery.getAttribute('data-filter');

   gallery.addEventListener('click',(e)=>{
        // console.log(datafilter);
        if(datafilter === 'all') {
            removeactiveitem();
            e.target.classList.add('activeitems');

            filternews.forEach(filternew =>{
                filternew.style.display='inline-block';
            });
            
            filterfrees.forEach(filterfree =>{
                filterfree.style.display='inline-block';
            });

            filterpros.forEach(filterpro =>{
                filterpro.style.display='inline-block';
            });
        }else if(datafilter === 'new') {
            removeactiveitem();
            e.target.classList.add('activeitems');

            filternews.forEach(filternew =>{
                filternew.style.display='inline-block';
            });
            
            filterfrees.forEach(filterfree =>{
                filterfree.style.display='none';
            });

            filterpros.forEach(filterpro =>{
                filterpro.style.display='none';
            });
        }else if(datafilter === 'free') {
            removeactiveitem();
            e.target.classList.add('activeitems');

            filternews.forEach(filternew =>{
                filternew.style.display='none';
            });
            
            filterfrees.forEach(filterfree =>{
                filterfree.style.display='inline-block';
            });

            filterpros.forEach(filterpro =>{
                filterpro.style.display='none';
            });
        }else {
            removeactiveitem();
            e.target.classList.add('activeitems');

            filternews.forEach(filternew =>{
                filternew.style.display='none';
            });
            
            filterfrees.forEach(filterfree =>{
                filterfree.style.display='none';
            });

            filterpros.forEach(filterpro =>{
                filterpro.style.display='inline-block';
            });
        }
   });

});

function removeactiveitem() {
    gallerylist.forEach(gallery=>{
        gallery.classList.remove('activeitems');
    });
};

const getyear=document.getElementById('getyear');
var getfullyear=new Date().getFullYear();

getyear.textContent=getfullyear;